@echo off
rem -------------------------------------------------------------
rem  탐색기 오른쪽 클릭으로 MP3변환하기 설정
rem -------------------------------------------------------------
set FFMPEG_FOLDER=C:\ffmpeg4

pushd "%~dp0"
echo .
echo .
echo =====================================
echo    FFMPEG 다운로드  
echo =====================================
:FFMPEG_Q
set /p FYN=FFmpeg을 다운 받겠습니까? (Y/N)?
if /i "%FYN%" == "y" goto FFMPEG_DOWN
if /i "%FYN%" == "n" goto GIT_SRC
goto FFMPEG_Q

:FFMPEG_DOWN
call c:\windows\explorer.exe https://ffmpeg.zeranoe.com/builds/win64/static/ffmpeg-4.0.2-win64-static.zip
echo .
echo .
echo   다운받은 FFMEPG을 %FFMPEG_FOLDER% 에 압축을 풀고 아무키나 누르세요
echo .
pause


:GIT_SRC
echo .
echo .
echo =====================================
echo    2mpx 소스 다운받기
echo =====================================
:GIT_SRC_Q
set /p GSYN=2mpx 소스를 다운 받겠습니까? (Y/N)?
if /i "%GSYN%" == "y" goto GIT_SRC_DOWN
if /i "%GSYN%" == "n" goto END
goto GIT_SRC_Q

:GIT_SRC_DOWN
git clone https://bitbucket.org/ianguylee/2mpx.git

:END
call 2mpx\update.bat
