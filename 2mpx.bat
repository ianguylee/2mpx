@echo off
rem chcp 949
chcp 65001
rem #################################################################
rem                         SET variables 
rem #################################################################
rem --- office
SET FFMPEG_FOLDER=C:\ffmpeg4\bin
rem SET QUEUE_FOLDER=D:\_Cloud_\Dropbox\Lab\ffmpeg\queue

rem --- home
rem SET FFMPEG_FOLDER=X:\Source\Util\av\ffmpeg-2016-win64\bin
pushd "%~dp0"
SET QUEUE_FOLDER=queue
rem --- end

SET FFMPEG=%FFMPEG_FOLDER%\ffmpeg.exe -hide_banner
SET FFPROB=%FFMPEG_FOLDER%\ffprobe.exe -hide_banner



SET QFILETIME=%time:~0,2%_%time:~3,2%_%time:~6,5%
SET QFILE="%QUEUE_FOLDER%\%date%_%QFILETIME%_%~n1.txt"
SET WORKING_MARK=%QUEUE_FOLDER%\doing.log

SET DEST_EXT=mp4
IF NOT "%3" == "" SET DEST_EXT=%3

rem =================================================================

IF "%2" == "DIRECT"     GOTO STAGE_DIRECT_CONVERT
IF "%2" == "INFO"       GOTO STAGE_FILE_INFO
IF "%2" == "QUEUE_INIT" GOTO QUEUE_INIT


rem #################################################################
rem                         QUEUE the file
rem #################################################################
:STAGE_QUEUE
IF NOT '%1' == '' echo %1 > %QFILE%

rem =================================================================
:lockedAppend
2>nul (
  >queue.lock (
    IF EXIST "%WORKING_MARK%" GOTO STAGE_END    
    echo %date% %time% Start > %WORKING_MARK%    
  )
)||goto :lockedAppend
rem =================================================================

rem if previous work is in processing this work goto end


rem #################################################################
rem                     Queued Convert
rem #################################################################
:STAGE_QUEUED_CONVERT

rem count queued file
set filenum=0
for /r %QUEUE_FOLDER% %%f in (*.txt) do set /a filenum+=1

rem if has no queued file goto end
IF %filenum% == 0 (
	del %WORKING_MARK%
  del queue.lock
	goto STAGE_END
)

rem find queued file
for /r %QUEUE_FOLDER% %%q in (*.txt) do (
  echo.
  echo.
  echo ==================================================================
  echo %%q 
  echo ------------------------------------------------------------------
  for /f "usebackq delims=" %%i in ("%%q") do (
    %FFMPEG% -loglevel info -i %%i -threads 3 -ab 320k "%%~dpi%%~ni.%DEST_EXT%" 
  )
  rem rename queued file to done
  rem ren "%%q" "%%~nq.done"
  del "%%q"
)
GOTO STAGE_QUEUED_CONVERT

rem #################################################################
rem                     Direct Convert
rem #################################################################
:STAGE_DIRECT_CONVERT

%FFMPEG% -loglevel info -i %1 -threads 3 -ab 320k "%~dp1%~n1.%DEST_EXT%"
echo.
echo.

GOTO STAGE_END
rem #################################################################
rem                     File Info
rem #################################################################
:STAGE_FILE_INFO
%FFPROB% -i %1
pause
GOTO STAGE_END

rem #################################################################
rem                     Queue Initialize
rem #################################################################
:QUEUE_INIT
chcp 949

:QUEUE_INIT_Q
set /p FYN=변환 큐를 초기화 하시겠습니까? (Y/N)?
if /i "%FYN%" == "y" goto QUEUE_INIT_R
if /i "%FYN%" == "n" goto STAGE_END
goto QUEUE_INIT_Q

:QUEUE_INIT_R
del /Q %QUEUE_FOLDER%\*.*
echo.
echo.
echo 초기화 완료 하였습니다.
echo.
pause
GOTO STAGE_END

:STAGE_END
rem pause
