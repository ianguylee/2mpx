@echo off
setlocal enabledelayedexpansion
rem ########################### EID HERE ###########################################
SET IN_BAT_PATH=%1
set IN_BAT_PATH=%IN_BAT_PATH:"=%
set BAT_PATH=%IN_BAT_PATH:\=\\%
rem ################################################################################

SET REGFILE=convert_gen.reg
SET HKLM_SHELL_KEY=HKEY_LOCAL_MACHINE\SOFTWARE\Microsoft\Windows\CurrentVersion\Explorer\CommandStore\shell
SET to_mpx_bat=%BAT_PATH%2mpx.bat

rem ########################### EID HERE ###########################################
rem SET SUB_COMMAND=2mpx.Info;2mpx.DirectMp4;2mpx.QueueMp4;2mpx.DirectMp3;2mpx.QueueMp3
SET SUB_COMMAND=2mpx.QueueMp3;2mpx.Info; ;2mpx.QueueInit

SET TITLE["2mpx.Info"]="음원 정보"
SET COMMD["2mpx.Info"]="%to_mpx_bat% \"%%V\" INFO"

rem SET TITLE["2mpx.DirectMp4"]=".mp4 : Convert Directly"
rem SET COMMD["2mpx.DirectMp4"]="%to_mpx_bat% \"%%V\" DIRECT"

rem SET TITLE["2mpx.QueueMp4"]=".mp4 : Add to Queue"
rem SET COMMD["2mpx.QueueMp4"]="%to_mpx_bat% \"%%V\" QUEUE"

rem SET TITLE["2mpx.DirectMp3"]=".mp3 : Convert Directly"
rem SET COMMD["2mpx.DirectMp3"]="%to_mpx_bat% \"%%V\" DIRECT mp3"

SET TITLE["2mpx.QueueMp3"]="MP3 변환"
SET COMMD["2mpx.QueueMp3"]="%to_mpx_bat% \"%%V\" QUEUE mp3"


SET TITLE["2mpx.QueueInit"]="큐 초기화"
SET COMMD["2mpx.QueueInit"]="%to_mpx_bat% QUEUE_INIT QUEUE_INIT"

rem ################################################################################
SET SUB_CMD_LIST=%SUB_COMMAND%
SET SUB_CMD_LIST=%SUB_CMD_LIST:;= %
SET SUB_CMDSTR=%SUB_COMMAND: =^|%

echo Windows Registry Editor Version 5.00  > %REGFILE%
echo.                                      >> %REGFILE%
echo [HKEY_CLASSES_ROOT\*\shell\2MpX]      >> %REGFILE%
echo "MUIVerb"="MP3 변환하기"      			>> %REGFILE%
echo "Icon"="%BAT_PATH%monkey3.ico"      	>> %REGFILE%
echo "SubCommands"="%SUB_CMDSTR%"         	>> %REGFILE%
echo "SeparatorBefore"=""        			>> %REGFILE%
echo "SeparatorAfter"=""         			>> %REGFILE%
echo.                                      	>> %REGFILE%

for %%j in ( %SUB_CMD_LIST% ) do call :fn_wirte %%j

goto end

:fn_wirte
	echo %1
	SET KEY=%1
	SET TTL=!TITLE["%KEY%"]!
	SET COMMD=!COMMD["%KEY%"]!

	echo %KEY%
	echo %TTL%
	echo %COMMD%
	rem ===== to file
	echo [%HKLM_SHELL_KEY%\%KEY%]                >> %REGFILE%
	echo @=%TTL%                               >> %REGFILE%
	echo.                                      >> %REGFILE%
	echo [%HKLM_SHELL_KEY%\%KEY%\command]        >> %REGFILE%
	echo @=%COMMD%                              >> %REGFILE%
	echo.                                      >> %REGFILE%
	echo. 
:end
endlocal