@echo off
rem -------------------------------------------------------------
rem  탐색기 오른쪽 클릭으로 MP3변환하기 설정
rem -------------------------------------------------------------
set FFMPEG_FOLDER=C:\ffmpeg4

pushd "%~dp0"

echo =====================================
echo    최신 소스받기
echo =====================================
:GIT_PULL_Q
set /p REGYN=최신 소스로 업데이트 하시겠습니까? (Y/N)?
if /i "%REGYN%" == "y" goto GIT_PULL
if /i "%REGYN%" == "n" goto MAKE_QUEUE_FOLDER
goto RUN_REG_Q

:GIT_PULL
git pull

:MAKE_QUEUE_FOLDER
IF NOT EXIST queue mkdir queue

:REGFILE
echo .
echo .
echo =====================================
echo    레지스트리 등록
echo =====================================
:RUN_REG_Q
set /p REGYN=레지스트리를 시스템에 등록 하시겠습니까? (Y/N)?
if /i "%REGYN%" == "y" goto RUN_REGFILE
if /i "%REGYN%" == "n" goto END
goto RUN_REG_Q

:RUN_REGFILE
call make_reg.bat "%~dp0"
call convert_gen.reg
rem del convert_gen.reg
:END
pause